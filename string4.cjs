

let validName = {
    "first_name": "first",
    "middle_name": "middle",
    "last_name": "last"
};

function string4(testObject) {
    let fullName = '';
    for (let key in validName) {
        if (testObject.hasOwnProperty(key)) {
            let name = testObject[key];
            name = name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
            fullName += " " + name;
        }
    }
    return fullName.trim();
}

module.exports = string4;