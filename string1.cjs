function string1(testString) {

    let regex = /[^0-9.-]/g;
    let modifiedNum = testString.replace(regex, '');

    if (isNaN(modifiedNum)) {
        return 0;
    }
    return Number(modifiedNum);
}

module.exports = string1;

