function string2(testString) {
    let ipArray = testString.split('.');
    if (ipArray.length === 4) // bcoz ip address have only 4 sections 
    {
        for (let index = 0; index < 4; index++) {
            if (isNaN(ipArray[index])) {
                return [];
            }
            if (ipArray[index] > 255 || ipArray[index] < 0) // ip address of each section has range betweenn 0 to 255
            {
                return [];
            }
            ipArray[index] = Number(ipArray[index]);
        }
        return ipArray;
    }
    return [];
}


module.exports = string2;